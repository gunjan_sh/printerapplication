//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PrintApplication.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class DocumentMapper
    {
        public int DocMapperId { get; set; }
        public Nullable<int> UserId { get; set; }
        public string DocumentEmail { get; set; }
        public byte[] DocumentPassword { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string ContactNumber { get; set; }
        public Nullable<System.Guid> DocumentIdentifier { get; set; }
    
        public virtual User User { get; set; }
    }
}
