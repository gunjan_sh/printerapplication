﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintApplication.Database
{
    public interface IDataContext
    {
        int SaveChanges();
        IDbSet<TEntity> Set<TEntity>() where TEntity : class;
    }
}
