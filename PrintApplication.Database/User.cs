//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PrintApplication.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class User
    {
        public User()
        {
            this.DocumentDownloads = new HashSet<DocumentDownload>();
            this.DocumentMappers = new HashSet<DocumentMapper>();
        }
    
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public byte[] Password { get; set; }
        public int UserTypeId { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime LastLoggedInDate { get; set; }
    
        public virtual ICollection<DocumentDownload> DocumentDownloads { get; set; }
        public virtual UserType UserType { get; set; }
        public virtual ICollection<DocumentMapper> DocumentMappers { get; set; }
    }
}
