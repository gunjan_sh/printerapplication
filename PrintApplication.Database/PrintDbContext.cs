﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintApplication.Database
{
    public partial class Entities : IDataContext
    {
        public Entities(string connectionString) : this()
        { }

        int SaveChanges()
        {
            return this.SaveChanges();
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }
    }
}
