using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc4;
using BAL;
using PrintApplication.Database;
using PrintApplication.Common;
using System.Configuration;
using PrintApplication.DataContracts;

namespace PrintApplication
{
  public static class Bootstrapper
  {
    public static IUnityContainer Initialise()
    {
      var container = BuildUnityContainer();

      DependencyResolver.SetResolver(new Unity.Mvc4.UnityDependencyResolver(container));

      return container;
    }

    private static IUnityContainer BuildUnityContainer()
    {
      var container = new UnityContainer();

      // register all your components with the container here
      // it is NOT necessary to register your controllers

      // e.g. container.RegisterType<ITestService, TestService>();    
      RegisterTypes(container);

      return container;
    }

    public static void RegisterTypes(IUnityContainer container)
    {
            container.RegisterType<IBusinessManager, BusinessManager>();
            container.RegisterType<IDataContext, Entities>(new RequestLifetimeManager(), new InjectionConstructor(ConfigurationManager.ConnectionStrings["Entities"].ConnectionString));
    }

  }
}