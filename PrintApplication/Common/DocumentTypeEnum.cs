﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrintApplication.Common
{
    public enum DocumentTypeEnum
    {
        Doc = 1,
        Docx,
        Pdf,
        Ppt,
        Txt,
        Xls,
        Xlsx
    }
}