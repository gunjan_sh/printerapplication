﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrintApplication.Common
{
    public class RequestLifetimeManager : LifetimeManager, IDisposable
    {
        private readonly Guid objectKey = Guid.NewGuid();

        public override object GetValue()
        {
            if(HttpContext.Current != null && HttpContext.Current.Items.Contains(this.objectKey))
            {
                return HttpContext.Current.Items[this.objectKey];
            }
            return null;
        }

        public override void RemoveValue()
        {
            if(HttpContext.Current != null)
            {
                var item = this.GetValue() as IDisposable;
                if(item != null)
                {
                    item.Dispose();
                }

                HttpContext.Current.Items.Remove(this.objectKey);
            }
        }

        public override void SetValue(object newValue)
        {
            if(HttpContext.Current != null)
            {
                HttpContext.Current.Items[this.objectKey] = newValue;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(disposing)
            {
                this.RemoveValue();
            }
        }
    }
}