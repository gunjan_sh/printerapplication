﻿using BAL;
using PrintApplication.Common;
using PrintApplication.Database;
using PrintApplication.DataContracts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace PrintApplication.Controllers
{
    public class UploaderController : BaseController
    {
        public readonly IBusinessManager _businessMgr;

        // GET: /Uploader/
        public UploaderController(IDataContext dbContext)
        {
            _businessMgr = new BusinessManager(dbContext);
        }

        public string Get()
        {
            return "This api is for Print Application";
        }

        [HttpPost]
        public string Upload(string txtEmail, string txtContact, string txtPassword)
        {
            var files = HttpContext.Current.Request.Files;
            var identifier = string.Empty;
            if (files.Count != 0)
            {
                identifier = SaveFiles(files, txtEmail, txtContact, txtPassword);
            }

            return identifier;
        }

        private string SaveFiles(HttpFileCollection httpFiles, string txtEmail, string txtContact, string txtPassword)
        {
            var uploadPath = ConfigurationManager.AppSettings["fileUploadPath"];
            var docIdentifier = Guid.NewGuid();

            var dirPath = Path.Combine(uploadPath, docIdentifier.ToString());
            var directoryInfo = Directory.CreateDirectory(dirPath);

            foreach (string file in httpFiles)
            {
                var fs = HttpContext.Current.Request.Files.Get(file) as HttpPostedFile;
                //var fs = Request.Files[file] as HttpPostedFileBase;
                if (fs.ContentLength > 0)
                {
                    var fileFullName = Path.GetFileName(fs.FileName);
                    var fileName = Path.Combine(dirPath, fileFullName);
                    fs.SaveAs(fileName);

                    var fileExtension = Path.GetExtension(fileFullName).Substring(1);
                    //TO DO: Need to add validation for supported document types
                    var docType = (DocumentTypeEnum)Enum.Parse(typeof(DocumentTypeEnum), fileExtension, true);

                    var document = new Document()
                    {
                        DocumentName = fileFullName,
                        Identifier = docIdentifier,
                        Path = Path.GetFullPath(fileName),
                        DocumentTypeId = (int)docType,
                        IsActive = true,
                        CreatedDate = DateTime.Now
                    };

                    this._businessMgr.SaveDocument(document);
                }
            }

            var documentMapper = new DocumentMapper
            {
                UserId = null, //TO DO: Use User Id
                DocumentEmail = txtEmail, //TO DO: If logged in, and email not provided, pick user emailId
                DocumentIdentifier = docIdentifier,
                ContactNumber = txtContact, //TO DO: If logged in, and contact not provided, pick user contact number
                DocumentPassword = Encoding.UTF8.GetBytes(txtPassword),
                CreatedDate = DateTime.Now
            };

            this._businessMgr.CreateDocumentMapper(documentMapper);

            return docIdentifier.ToString();
        }
    }
}
