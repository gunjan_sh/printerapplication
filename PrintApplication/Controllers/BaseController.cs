﻿using PrintApplication.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PrintApplication.Controllers
{
    public class BaseController : ApiController
    {
        public BaseController()
        {

        }

        public BaseController(IDataContext dbContext)
        {
            this.DataContext = dbContext;
        }

        protected IDataContext DataContext
        {
            get;
            private set;
        }
    }
}
