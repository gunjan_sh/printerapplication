﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
//using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using PrintApplication.Common;
using PrintApplication.Database;
using System.Configuration;
using BAL;
using PrintApplication.DataContracts;

namespace PrintApplication
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        private static UnityContainer _container;

        public static IUnityContainer UnityContainer
        {
            get
            {
                if(_container == null)
                {
                    _container = new UnityContainer();
                    InitialzeUnity(_container);
                }
                return _container;
            }
        }

        private static void InitialzeUnity(UnityContainer container)
        {
            container.RegisterType<IBusinessManager, BusinessManager>();
            container.RegisterType<IDataContext, Entities>(new RequestLifetimeManager(), new InjectionConstructor(ConfigurationManager.ConnectionStrings["Entities"].ConnectionString));            
        }

        protected void Application_Start()
        {
            //AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            //FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(MvcApplication.UnityContainer);
            //DependencyResolver.SetResolver(new Unity.WebApi.UnityDependencyResolver(MvcApplication.UnityContainer));
            //Bootstrapper.Initialise();
        }
    }
}