﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PrintApplication.DataContracts
{
    public interface IRepository<Entity> where Entity: class
    {
        void Add(Entity entity);
        void Update(Entity entity);
        void Delete(Entity entity);
        void Delete(Expression<Func<Entity, bool>> criteria);
        IEnumerable<Entity> GetAll();
        IEnumerable<Entity> GetAll(Expression<Func<Entity, bool>> criteria);
        int SavChanges();
    }
}
