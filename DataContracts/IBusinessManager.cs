﻿using PrintApplication.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintApplication.DataContracts
{
    public interface IBusinessManager
    {
        IEnumerable<string> GetDocumentNames();

        IEnumerable<DocumentType> GetDocumentTypes();

        void SaveDocument(Document document);

        void CreateDocumentMapper(DocumentMapper docMapper);
    }
}
