﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrintApplication.Database;
using PrintApplication.DataContracts;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;


namespace PrintApplication.Repositories
{
    public class BaseRepository<Entity> : IRepository<Entity> where Entity : class
    {
        protected IDbSet<Entity> _dBSet { get; set; }

        protected IDataContext _dbContext { get; set; }

        public BaseRepository(IDataContext dbContext)
        {
            _dbContext = dbContext;
            _dBSet = dbContext.Set<Entity>();
        }

        public void Add(Entity entity)
        {
            this._dBSet.Add(entity);
        }

        public void Update(Entity entity) { }

        public void Delete(Entity entity) { }

        public void Delete(Expression<Func<Entity, bool>> criteria) { }

        public IEnumerable<Entity> GetAll()
        {
            return this._dBSet;
        }

        public IEnumerable<Entity> GetAll(Expression<Func<Entity, bool>> criteria)
        {
            return this._dBSet.Where(criteria);
        }

        public int SavChanges()
        {
            return this._dbContext.SaveChanges();
        }
    }
}
