﻿using PrintApplication.Database;
using PrintApplication.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintApplication.Repositories
{
    public class UserRepository : BaseRepository<User> , IUserRepository
    {
        public UserRepository(IDataContext dbcontext) : base(dbcontext)
        {

        }
    }
}
