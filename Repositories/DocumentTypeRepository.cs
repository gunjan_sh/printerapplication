﻿using PrintApplication.Database;
using PrintApplication.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintApplication.DataContracts
{
    public class DocumentTypeRepository : BaseRepository<DocumentType> , IDocumentTypeRepository
    {
        public DocumentTypeRepository(IDataContext dbContext): base(dbContext)
        { }
    }
}
