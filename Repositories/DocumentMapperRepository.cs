﻿using PrintApplication.Database;
using PrintApplication.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintApplication.Repositories
{
    public class DocumentMapperRepository : BaseRepository<DocumentMapper>, IDocumentMapperRepository
    {
        public DocumentMapperRepository(IDataContext dataContext) : base(dataContext)
        {

        }
    }
}
