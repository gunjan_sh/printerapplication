﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrintApplication.DataContracts;
using PrintApplication.Database;
using System.Data.Entity;

namespace PrintApplication.Repositories
{
    public class DocumentRepository : BaseRepository<Document>, IDocumentRepository
    {
        public DocumentRepository(IDataContext dbContext) : base(dbContext)
        {

        }
    }
}
