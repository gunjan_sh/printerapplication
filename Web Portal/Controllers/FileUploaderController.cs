﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using file = System.IO;

namespace Web_Portal.Controllers
{
    public class FileUploaderController : Controller
    {
        // GET: FileUploader
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Upload(string txtEmail, string txtContact, string txtPassword)
        {
            int filesCount = Request.Files.Count;
            var identifier = string.Empty;

            if (filesCount != 0)
            {
                identifier = this.SaveFiles(Request.Files, txtEmail, txtContact, txtPassword);
            }

            return Json(new { Identifier = identifier }, JsonRequestBehavior.AllowGet);
        }

        private string SaveFiles(HttpFileCollectionBase files, string txtEmail, string txtContact, string txtPassword)
        {
            var baseUri = "http://localhost:59785/api/Uploader/Upload/?";
            var apiCaller = string.Format("{0}txtEmail={1}&txtContact={2}&txtPassword={3}", baseUri, txtEmail, txtContact, txtPassword);

            var formData = new MultipartFormDataContent();
            var uploadPath = @"D:\VS Projs\Print\UploadedDocs\";

            foreach(string fileBase in files)
            {
                var fs = Request.Files.Get(fileBase);
                if (fs.ContentLength > 0)
                {
                    var filePath = string.Format("{0}{1}", uploadPath, fs.FileName);
                    fs.SaveAs(filePath);

                    var content = new StreamContent(file.File.OpenRead(filePath));
                    formData.Add(content, fs.FileName, fs.FileName);
                }
            }

            using (var client = new HttpClient())
            {
                var response = client.PostAsync(apiCaller, formData).Result;

                if(response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    return "Error";
                }
            }
        }
    }
}