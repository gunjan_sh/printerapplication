﻿//using PrintApplication.DataContracts;
using PrintApplication.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using PrintApplication.DataContracts;
using PrintApplication.Database;
//using System.Data.Entity;

namespace BAL
{
    public class BusinessManager : IBusinessManager
    {
        private readonly IDocumentRepository documentRepository;
        private readonly IDocumentTypeRepository documentTypeRepository;
        private readonly IDocumentMapperRepository documentMapperRepository;

        public BusinessManager(IDataContext dbContext)
        {
            documentRepository = new DocumentRepository(dbContext);
            documentTypeRepository = new DocumentTypeRepository(dbContext);
            documentMapperRepository = new DocumentMapperRepository(dbContext);
        }

        public IEnumerable<string> GetDocumentNames()
        {
            var documents = this.documentRepository.GetAll().Select(x => x.DocumentName);
            return documents.ToList();
        }

        public IEnumerable<DocumentType> GetDocumentTypes()
        {
            var documents = this.documentTypeRepository.GetAll();
            return documents.ToList();
        }

        public void SaveDocument(Document document)
        {
            this.documentRepository.Add(document);
            this.documentRepository.SavChanges();
        }

        public void CreateDocumentMapper(DocumentMapper docMapper)
        {
            this.documentMapperRepository.Add(docMapper);
            this.documentMapperRepository.SavChanges();
        }
    }
}
